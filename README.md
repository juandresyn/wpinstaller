
# Wordpress Files Installer #

Simple installer for wordpress file, avoid the annoying process of files and upload and uncompress etc.

### What is this repository for? ###

* Wordpress easy instalation on server

### Requirements ###

* Any Linux hosting with linux console.

### How do I get set up? ###

* Select a lenguaje
* if you want to install in another folder insert the folder name, by default, it istall wordpress on the current folder.
* Press Install WordPress.

### Screenshot ###
http://juandresyn.com/wp-content/uploads/2014/07/Sin-t%C3%ADtulo-1024x628.png

### Contribution ###

* http://twitter.com/Alejandrogih
* http://twitter.com/JuandresYN

### License ###

General Public License (GPL)