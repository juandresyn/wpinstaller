<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>WPinstaller! - By Do0KieM4N & JuandresYN</title>
<link rel="shortcut icon" href="http://s.w.org/favicon.ico" type="image/x-icon" />
</head>
<body>
<style type="text/css"> 
	html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
	font: 16px 'Open Sans', sans-serif;
	color: #0d0d0d
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

h1 {
	font-size: 35px;
}

h2 {
	font-size: 30px;
}

h3 {
	font-size: 27px;
}

h4 {
	font-size: 23px;
}

h5 {
	font-size: 18px;
}

h6 {
	font-size: 14px;
}

strong, b {
	font-weight: bold;
}

p{
	font-size: 16px;
}

/*Hack for reset height*/

.clear{
	clear: both;
}

header{
	background-color: #2B3038; 
	padding: 15px;
	padding-bottom: 30px;
	color: #fafafa;
}

header .container{
	padding-top: 0;
	float: none;
}

header h2{
	float: left;
	margin: -15px;
}

header .change{
	float: right;
	width: 15%;	
	margin: -5px;
}

header .change .top-lang{
	float: right;
	margin-left: 5%;
}

.wrapper{
	width: 100%;
	height: 100%;
}

.container{
	width: 1000px;
	margin: auto;
	padding-top: 30px;
}

.social, .content{
	float: left;
	margin: auto;
}

.social{
	width: 20%;
	margin-top: 10%;
}

.social ul li.git img{
	width: 85px;
	height: auto;
}

.social ul li.bit img{
	width: 86px;
	height: auto;
}

.social h4{
	color: rgb(15, 139, 221);
}

.content{
	width: 78%;
	margin-left: 2%;
}

.content .title{
	margin: auto;
	text-align: center;
	margin-bottom: 25px;
}

.content .title h1{
	color: #000;
	font-family: 'arial', sans-serif;
	font-size: 40px;
}

.content .description{
	text-align: justify;
	font-size: 16px;
	margin-bottom: 20px;
}

.content .description .learn{
	float: right;
}

.content .lang{
	width: 50%;
	margin: auto;
}

.content .lang ul li{
	width: 47%;
	height: auto;
	float: left;
	margin-left: 6%;
	margin-bottom: 20px;
}

.content .lang ul li:first-child{
	margin-left: 0%;
}

.content .lang .lang-option{
	width: 40%;
	height: auto;
	margin: auto;
	text-align: center;
}

input[type="radio"] {
	float: left;
	margin-top: 15px;
}

.content .lang li:last-child .lang-option{
	padding-top: 25px;
}

.content .lang .lang-option img{
	width: 100%;
	height: auto;
}

.download{
	width: 80%;
	margin: auto;
	height: auto;
}

.download input[type="text"], .download input[type="submit"]{
	float: left;
}

.download input[type="text"]{
	height: 35px;
	width: 62%;
	padding-left: 10px;
	font-size: 18px;
	color: #0d0d0d;
	background-color: #e3e3e3;
	border: 2px solid #bababa;
	border-radius: 2px;
	font-family: 'Open Sans', sans-serif;
}

.download input[type="submit"]{
	height: 41px;
	width: 30%;
	margin-left: 5%;
	background-color: #1FBAA5;
	border: none;
	border-radius: 2px;
	color: #fff;
	font-size: 20px;
}

.download p{
	font-size: 14px;
	color: #8b8b8b;
	padding-left: 5px;
}

footer{
	background-color: #2F3037;
	padding: 10px;
	position: fixed;
	bottom: 0;
	font-size: 18px;
	color: #f0f0f0;	
	width: 100%;
}

footer .credits{
	float: right;
	width: 400px;
}

footer .credits a{
	color: #f0f0f0;
	text-decoration: underline;
	cursor: pointer;
}
</style>
<header>
		<div class="container">
		<h2>WPinstaller!</h2>
		<div class="change">
			<!--<div class="top-lang"><a href="#">ES</a></div>
			<div class="top-lang">|</div>
			<div class="top-lang"><a href="#">EN</a></div>-->
		</div><!--change-->
	</div><!--container-->
</header>
<div class="wrapper">
	<div class="container">
		<aside class="social">
			<ul>
				<li class="git"><a href="https://github.com/Permalinkgroup/wpinstaller" target="_blank"><img src="https://assets-cdn.github.com/images/modules/logos_page/Octocat.png"></a></li>
				<li class="bit"><a href="#" target="_blank"><img src="http://blog.poeditor.com/wp-content/uploads/2014/06/bitbucket-logo.png"></a></li>
				<li><h4>Follow us on Tw</h4></li>
				<br>
				<li><a href="https://twitter.com/alejandrogih" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @alejandrogih</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
				<p>
					  

				</p>
				<li><a href="https://twitter.com/JuandresYN" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @JuandresYN</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
			</ul>
		</aside>
		<div class="content">
			<div class="info">
				<div class="title">
					<h1>WPinstaller!</h1>
				</div><!--title-->
				<div class="description">
					<p>
						WPinstaller!, is a just one file installer for the Latest version of wordpress.
						You dont need to download, decompress, create the folder, upload and wait.
						With WPinstaller just upload this file, choose your language, 
						the folder where you whish to install wordpress, 
						(blank is you dont want to create any folder, "/" or "root" by default).
						Reload the page, (if you insert a folder name, go to that path)...
					</p>
					<p>
						AND WALÁ!
					</p>
					<a href="http://permalinkgroup.com/blog/?p=5" target="_blank" class="learn">Learn More</a>
				</div><!--description-->
			</div><!--info-->
			<div class="lang">
			<form action="" method="post">
				<ul>
					<li><input type="radio" name="lang" value="es"><h2>Espa&ntilde;ol</h2></li>
					<li><input type="radio" name="lang" value="en"><h2>English</h2></li>
				</ul>
			<div class="clear"></div>
			</div><!--lang-->
			<div class="download">
					<input type="text" name="dir" placeholder="Which directory want to install?">
					<input type="submit" name="download" value="Install WordPress">
					<p>"/"  or "root", by default.</p>
				</form>
				<div class="clear"></div>
			</div>
			</div>
		</div><!--content-->
		<div class="clear"></div>
	</div><!--container-->
</div><!--wrapper-->
<footer>
	<div class="credits"><p>By <a href="http://twitter.com/alejandrogih" target="_blank">Do0KieM4N</a> & <a href="http://twitter.com/juandresyn" target="_blank">JuandresYN</a></p></div>
</footer>
</body>
</html>
<?php

if (isset($_POST['lang']) && $_POST['lang'] == 'en') {

		$dir = $_POST['dir'];

        if(isset($dir)){
		shell_exec('mkdir ' . $dir);
		$targetdir = getcwd() . '/' . $dir;
	}else{
		$targetdir = getcwd();
	}

	shell_exec ('wget http://wordpress.org/latest.zip');
	shell_exec ('unzip latest.zip');
	shell_exec ('mv '. getcwd() . '/wordpress/* ' . $targetdir);
	shell_exec ('find ' . $targetdir . ' -type d -exec chmod 755 {} \;' );
	shell_exec ('find ' . $targetdir . ' -type f -exec chmod 644 {} \;' );
	shell_exec ('rm -r ' . getcwd() . '/wordpress/');
	shell_exec ('rm ' . getcwd() . '/latest.zip');
	shell_exec ('rm ' . getcwd() . '/wpinstaller.php');

	echo "";

    } elseif (isset($_POST['lang']) && $_POST['lang'] == 'es') {
     
     $dir = $_POST['dir'];

    if(isset($dir)){
		shell_exec('mkdir ' . $dir);
		$targetdir = getcwd() . '/' . $dir;
	}else{
		$targetdir = getcwd();
	}

	shell_exec ('wget http://es.wordpress.org/wordpress-3.9.1-es_ES.zip');
	shell_exec ('unzip wordpress-3.9.1-es_ES.zip');
	shell_exec ('mv '. getcwd() . '/wordpress/* ' . $targetdir);
	shell_exec ('find ' . $targetdir . ' -type d -exec chmod 755 {} \;' );
	shell_exec ('find ' . $targetdir . ' -type f -exec chmod 644 {} \;' );
	shell_exec ('rm -r ' . getcwd() . '/wordpress/');
	shell_exec ('rm ' . getcwd() . '/wordpress-3.9.1-es_ES.zip');
	shell_exec ('rm ' . getcwd() . '/wpinstaller.php');


    }
?>
